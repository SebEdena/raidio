import dotenv from "dotenv";
import fs from "fs";
import path from "path";
dotenv.config();

export interface RadioData {
    name: string,
    url: string,
    genre: string,
    language: string,
    bitrate: number
}

export class RadioRepository {

    private static instance: RadioRepository;

    private _radiosPath: string;

    private radios: RadioData[] | null = null;

    private loadPromise: Promise<unknown>;

    private dataReady = false;

    private constructor(radiosPath : string) {
        this._radiosPath = radiosPath;
        this.loadPromise = this.parseRadioJson();

        fs.watchFile(path.join(process.cwd(), this._radiosPath), { "interval": 1000 }, () => {
            this.loadPromise = this.parseRadioJson();
        });
    }

    public static init(radiosPath : string): Promise<void> {
        try {
            RadioRepository.instance = new RadioRepository(radiosPath);
            return Promise.resolve();
        } catch (error) {
            return Promise.reject(error);
        }
    }

    public static get(): RadioRepository {
        return RadioRepository.instance;
    }

    public findByName(radio: string) : Promise<RadioData[]> {
        return this.fetch(
            (radioElt: RadioData) => radioElt.name.includes(radio)
        );
    }

    private parseRadioJson(): Promise<unknown> {
        return new Promise((resolve, reject) => {
            this.dataReady = false;
            fs.readFile(path.join(process.cwd(), this._radiosPath), {}, (err, data) => {
                if (err) {
                    this.dataReady = true;
                    reject(err);
                } else {
                    try {
                        const array: RadioData[] = JSON.parse(data.toString());
                        this.radios = array;
                        this.dataReady = true;
                        resolve(true);
                    } catch (error) {
                        this.dataReady = true;
                        reject(error);
                    }
                }
            });
        });
    }

    private fetch(filter: (radio: RadioData) => boolean): Promise<RadioData[]> {
        return new Promise((resolve,) => {
            this.loadPromise.then(() => {
                while (!this.dataReady) {}
                if (this.radios === null) {
                    resolve([]);
                } else {
                    resolve(this.radios.filter(filter));
                }
            });
        });
    }
}
