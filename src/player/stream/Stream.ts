import { Readable } from "stream";

export default abstract class Stream {

    protected _stream: string;

    public constructor(stream: string) {
        this._stream = stream;
    }

    public abstract buildStream(): Readable | string;

}
