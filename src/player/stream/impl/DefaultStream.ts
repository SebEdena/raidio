import { Readable } from "stream";
import Stream from "../Stream";

export default class DefaultStream extends Stream {

    public constructor(stream: string) {
        super(stream);
    }

    public buildStream() : Readable | string {
        return this._stream.toString();
    }

}
