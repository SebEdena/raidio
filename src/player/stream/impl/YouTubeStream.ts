import { Readable } from "stream";
import Stream from "../Stream";
import ytdl from "ytdl-core";

export default class DefaultStream extends Stream {

    public constructor(stream: string) {
        super(stream);
    }

    public buildStream() : Readable | string {
        return ytdl(this._stream.toString());
    }

}
