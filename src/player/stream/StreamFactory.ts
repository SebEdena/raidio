import Stream from "./Stream";
import YouTubeStream from "./impl/YouTubeStream";
import DefaultStream from "./impl/DefaultStream";

const YOUTUBE_HOST = "www.youtube.com";

export default class StreamFactory {

    private static instance: StreamFactory;

    private constructor() { }

    public static get(): StreamFactory {
        if (!StreamFactory.instance) {
            StreamFactory.instance = new StreamFactory();
        }
        return StreamFactory.instance;
    }

    public getStream(source: URL): Stream {
        if (source.hostname === YOUTUBE_HOST) return new YouTubeStream(source.toString());
        return new DefaultStream(source.toString());
    }
}
