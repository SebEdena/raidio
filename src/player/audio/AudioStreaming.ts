import { Message, Speaking } from "discord.js";
import StreamFactory from "../stream/StreamFactory";
import Stream from "../stream/Stream";
import { StreamSource } from "../stream/StreamSource";

export default class AudioStreaming {

    private static instance: AudioStreaming;

    private constructor() {

    }

    public static init(): Promise<void> {
        try {
            AudioStreaming.instance = new AudioStreaming();
            return Promise.resolve();
        } catch (error) {
            return Promise.reject(error);
        }
    }

    public static get(): AudioStreaming {
        return AudioStreaming.instance;
    }

    public play(url : URL, source : StreamSource, displayName : string, message : Message) : void {

        // https://gabrieltanner.org/blog/dicord-music-bot

        const voiceChannel = message.member !== null ? message.member.voice.channel : null;
        if (!voiceChannel) {
            message.channel.send(
                "You need to be in a voice channel to play a radio!",
            );
        } else {
            voiceChannel.join()
                .then((connection) => {
                    const stream : Stream = StreamFactory.get().getStream(url);
                    connection
                        .play(stream.buildStream())
                        .once("start", () => {
                            switch (source) {
                                case StreamSource.URL: {
                                    message.channel.send(`Playing URL ${displayName}`);
                                    break;
                                }
                                case StreamSource.Radio: {
                                    message.channel.send(`Playing radio **${displayName}**`);
                                    break;
                                }
                                default: break;
                            }
                        });
                })
                .catch((error) => console.error(error));
        }
    }

    public pause(message : Message) :void {
        const voiceChannel = message.member !== null ? message.member.voice.channel : null;
        if (!voiceChannel) {
            message.channel.send(
                "You need to be in a voice channel to pause a radio.",
            );
        } else {
            voiceChannel.join()
                .then((connection) => {
                    try {
                        if (connection.speaking.has(Speaking.FLAGS.SPEAKING) && connection.dispatcher) {
                            connection.dispatcher.pause(true);
                            message.channel.send("Radio paused.");
                        } else {
                            message.channel.send("No radio is being currently played.");
                        }
                    } catch (error) {
                        console.error(error);
                    }
                })
                .catch((error) => console.error(error));
        }
    }

    public resume(message : Message) :void {
        const voiceChannel = message.member !== null ? message.member.voice.channel : null;
        if (!voiceChannel) {
            message.channel.send(
                "You need to be in a voice channel to resume a radio.",
            );
        } else {
            voiceChannel.join()
                .then((connection) => {
                    try {
                        if (connection.dispatcher && connection.dispatcher.paused) {
                            connection.dispatcher.resume();
                            message.channel.send("Radio resumed.");
                        } else {
                            message.channel.send("No radio is paused.");
                        }
                    } catch (error) {
                        console.error(error);
                    }
                })
                .catch((error) => console.error(error));
        }
    }
}
