import fs from "fs";
import path from "path";
import { Client, Collection, Message } from "discord.js";
import Command from "./Command";

export default class CommandHandler {

    private static instance: CommandHandler;

    private static commandPath: string = path.join(__dirname, "impl");

    private _commands: Collection<string, Command>;

    private _prefix: string;

    private constructor(client: Client, prefix: string) {
        this._prefix = prefix;
        this._commands = new Collection();
        this.initCommands(client);
    }

    public static init(client: Client, prefix: string): Promise<void> {
        try {
            CommandHandler.instance = new CommandHandler(client, prefix);
            return Promise.resolve();
        } catch (error) {
            return Promise.reject(error);
        }
    }

    public static get(): CommandHandler {
        return CommandHandler.instance;
    }

    get commands() : Collection<string, Command> {
        return this._commands;
    }

    get prefix() : string {
        return this._prefix;
    }

    private async initCommands(client: Client) {
        const files : string[] =
            fs.readdirSync(CommandHandler.commandPath).filter((file) => file.endsWith(".js"));

        const promises: Promise<Command>[] = files.map(async (file) => {
            const fileImport : Record<string, object> = await import(path.join(CommandHandler.commandPath, file));
            return fileImport.default as Command;
        });

        const commands = await Promise.all(promises);

        for (const command of commands) {
            this.commands.set(command.name, command);
        }

        client.on("message", (message) => {
            this.handleMessage(message);
        });
    }

    private parseArgs(argString: string) : string[] {
        let inQuote = false;
        let current = "";
        const args : string[] = [];

        for (let i = 0; i < argString.length; i++) {
            const nextChar = argString.charAt(i);
            if (nextChar === " ") {
                if (inQuote) {
                    current += nextChar;
                } else {
                    if (current !== "") args.push(current);
                    current = "";
                }
            } else if (nextChar === "\"") {
                if (current !== "") args.push(current);
                current = "";
                inQuote = !inQuote;
            } else {
                current += nextChar;
            }
        }

        if (current !== "") args.push(current);

        return args;
    }

    private handleMessage(message: Message) {
        if (!message.content.startsWith(this._prefix) || message.author.bot) {
            return;
        }

        const args = this.parseArgs(message.content.slice(this._prefix.length));
        const command = (args.shift() || "").toLowerCase();

        if (this.commands.has(command)) {
            const localCommand = this.commands.get(command);
            if (localCommand) localCommand.execute(message, args);
        } else {
            message.channel.send(`No command matches "${command}".`);
        }
    }
}
