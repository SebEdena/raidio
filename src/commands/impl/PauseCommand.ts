import { Message } from "discord.js";
import Command from "../Command";
import AudioStreaming from "../../player/audio/AudioStreaming";

const COMMAND = "pause";
const USAGE = "";
const DESCRIPTION = "Pause the radio.";
const SHOW_COMMAND = true;

export default new Command(
    COMMAND,
    USAGE,
    DESCRIPTION,
    SHOW_COMMAND,
    (message: Message, _args: string[]) => {
        AudioStreaming.get().pause(message);
    }
);
