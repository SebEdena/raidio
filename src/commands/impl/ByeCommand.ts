import { Message } from "discord.js";
import Command from "../Command";

const COMMAND = "bye";
const USAGE = "";
const DESCRIPTION = "Leaves the voice channel.";
const SHOW_COMMAND = true;

export default new Command(
    COMMAND,
    USAGE,
    DESCRIPTION,
    SHOW_COMMAND,
    (message: Message, _args: string[]) => {
        const voiceChannel = message.member !== null ? message.member.voice.channel : null;
        if (voiceChannel) {
            voiceChannel.leave();
        }
    }
);
