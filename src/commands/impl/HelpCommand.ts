import { Message } from "discord.js";
import CommandHandler from "../CommandHandler";
import Command from "../Command";

export default new Command(
    "help",
    "",
    "Prints all the commands with their usage.",
    true,
    (message: Message, _args: string[]) => {
        let messageString = "Here is the list of commands :\n";
        const commandHandler = CommandHandler.get();
        const commands = commandHandler.commands;

        for (const commandName of commands.keyArray().sort()) {
            const command = commands.get(commandName);
            if (typeof command !== "undefined" && command.display) {
                messageString += `**${commandHandler.prefix}${command.name}**`;
                messageString += `  ${command.usage}\n`;
                messageString += `\t\t${command.description}\n`;
            }
        }
        message.channel.send(messageString);
    }
);
