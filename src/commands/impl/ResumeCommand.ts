import { Message } from "discord.js";
import Command from "../Command";
import AudioStreaming from "../../player/audio/AudioStreaming";

const COMMAND = "resume";
const USAGE = "";
const DESCRIPTION = "Resume the radio.";
const SHOW_COMMAND = true;

export default new Command(
    COMMAND,
    USAGE,
    DESCRIPTION,
    SHOW_COMMAND,
    (message: Message, _args: string[]) => {
        AudioStreaming.get().resume(message);
    }
);
