import Command from "../Command";
import { Message } from "discord.js";

export default new Command(
    "ping",
    "",
    "Do a ping call to the raidio bot.",
    true,
    (message: Message, _args: string[]) => {
        message.channel.send("pong");
    }
);
