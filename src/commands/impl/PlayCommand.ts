import { Message } from "discord.js";
import Command from "../Command";
import CommandHandler from "../CommandHandler";
import { RadioRepository, RadioData } from "../../radios/RadioRepository";
import AudioStreaming from "../../player/audio/AudioStreaming";
import { StreamSource } from "../../player/stream/StreamSource";

const PREFIX = CommandHandler.get().prefix;
const COMMAND = "play";
const USAGE = "url | radio-name  [radio-number]";
const DESCRIPTION = "Play a radio from an url or a registered radio.";
const PROTOCOLS = ["http:", "https:"];
const RADIO_DISPLAY_LIMIT = 10;

function isValidUrl(urlString : string, protocols: string[]) : boolean {
    try {
        const url = new URL(urlString);
        if (protocols) {
            return protocols.includes(url.protocol);
        }
        return true;
    } catch (err) {
        return false;
    }
}

function checkValidRadioChoice(_args: string[], radios: RadioData[]) {
    const integerValue : number | null = _args.length >= 2 ? parseInt(_args[1], 10) : null;
    const result = {
        "integerValue": null as number | null,
        "error": "" as string
    };
    if (integerValue !== null) {
        if (isNaN(integerValue)) {
            result.error += "**ERROR : Radio number must be an integer !**\n";
        } else if (integerValue <= 0 || integerValue > radios.length) {
            result.error += `**ERROR : No radio for choice ${integerValue}. `;
            result.error += `The correct interval is 1-${radios.length}**\n`;
        } else {
            result.integerValue = integerValue;
        }
    }
    return result;
}

export default new Command(
    COMMAND,
    USAGE,
    DESCRIPTION,
    true,
    (message: Message, _args: string[]) => {
        if (_args.length === 0) {
            let messageString = "";
            messageString += `**${PREFIX}${COMMAND}** command needs **one url or string** argument.\n`;
            messageString += `Usage is : **${PREFIX}${COMMAND}**\t${USAGE}.`;
            message.channel.send(messageString);
            return;
        }
        if (isValidUrl(_args[0], PROTOCOLS)) {
            AudioStreaming.get().play(new URL(_args[0]), StreamSource.URL, _args[0], message);
        } else {
            RadioRepository.get().findByName(_args[0])
                .then((radios : RadioData[]) => {
                    switch (radios.length) {
                        case 0: {
                            let messageString = "";
                            messageString += `Unable to find the radio "${_args[0]}".\n`;
                            messageString += "Either the argument is an invalid url, either the radio does not exist.";
                            message.channel.send(messageString);
                            break;
                        }
                        case 1: {
                            AudioStreaming.get().play(new URL(radios[0].url), StreamSource.Radio, radios[0].name, message);
                            break;
                        }
                        default: {
                            const { integerValue, error } = checkValidRadioChoice(_args, radios);
                            if (!integerValue) {
                                let messageString = "";
                                messageString += error;
                                messageString += `**${radios.length} radios** found.\n`;
                                if (radios.length <= RADIO_DISPLAY_LIMIT) {
                                    for (let i = 0; i < radios.length; i++) {
                                        messageString += `\t${i + 1} : ${radios[i].name}\n`;
                                    }
                                    const radioFormat : string = _args[0].includes(" ") ? `"${_args[0]}"` : _args[0];
                                    messageString += `Select the radio using : **${PREFIX}${COMMAND}  ${radioFormat}  1-${radios.length}**`;
                                } else {
                                    messageString += "Too many radios to display, please update your query.\n";
                                    messageString += `The limit is **${RADIO_DISPLAY_LIMIT} radios**.`;
                                }
                                message.channel.send(messageString);
                            } else {
                                const chosenRadio = radios[integerValue - 1];
                                AudioStreaming.get().play(new URL(chosenRadio.url), StreamSource.Radio, chosenRadio.name, message);
                            }
                        }
                    }
                });
        }
    }
);
