import { Message } from "discord.js";

export default class Command {

    private readonly _name: string;

    private readonly _usage: string;

    private readonly _description: string;

    private readonly _display: boolean

    private readonly _execute: (message: Message, args: string[]) => void;

    public constructor(
        name: string,
        usage: string,
        description: string,
        display: boolean,
        execute: (message: Message, args: string[]) => void
    ) {
        this._name = name;
        this._usage = usage;
        this._description = description;
        this._display = display;
        this._execute = execute;
    }

    get name(): string {
        return this._name;
    }

    get usage(): string {
        return this._usage;
    }

    get description(): string {
        return this._description;
    }

    get display(): boolean {
        return this._display;
    }

    execute(message: Message, args: string[]): void {
        this._execute(message, args);
    }
}
