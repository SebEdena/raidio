import { Guild, TextChannel, Client } from "discord.js";

export default class GuildHandler {

    private static instance: GuildHandler;

    private _client : Client;

    private constructor(client : Client) {
        this._client = client;

        this._client.on("guildCreate", (guild) => this.welcome(guild));
    }

    public static init(client : Client): Promise<void> {
        try {
            GuildHandler.instance = new GuildHandler(client);
            return Promise.resolve();
        } catch (error) {
            return Promise.reject(error);
        }
    }

    public static get(): GuildHandler {
        return GuildHandler.instance;
    }


    public welcome(guild: Guild) : void {
        let channel = guild.systemChannel;

        if (channel === null) { // Attempt to find a correct text channel if no default
            const channels = guild.channels.cache;

            for (const [, value] of channels) {
                const channelType = value.type;
                if (channelType === "text") {
                    channel = value as TextChannel;
                    break;
                }
            }
        }

        if (channel !== null) {
            channel.send("Thanks for inviting me into this server!");
        }
    }
}
