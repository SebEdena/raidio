import dotenv from "dotenv";
import { Client } from "discord.js";

import { RadioRepository } from "./radios/RadioRepository";
import CommandHandler from "./commands/CommandHandler";
import AudioStreaming from "./player/audio/AudioStreaming";
import GuildHandler from "./guild/GuildHandler";

dotenv.config();

const token = process.env.token;
const prefix = process.env.prefix || "$";
const radiosJson = process.env.radios || "";

if (typeof token === "undefined") {
    throw new ReferenceError('[NO DISCORD API TOKEN] Discord API token not found in ".env" file. Exiting.');
}

const client = new Client();

Promise
    .all([
        CommandHandler.init(client, prefix),
        RadioRepository.init(radiosJson),
        AudioStreaming.init(),
        GuildHandler.init(client)
    ])
    .then(() => client.login(token))
    .then(() => {
        console.log("Bot ready for commands!");
        return Promise.resolve();
    })
    .catch((error) => console.error(error));
